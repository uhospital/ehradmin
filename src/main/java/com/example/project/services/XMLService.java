package com.example.project.services;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.SAXParserFactory;

import org.cyberborean.rdfbeans.annotations.RDFBean;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Service;

import com.example.project.rm.RMObject;
import com.example.project.rm.common.Composition;
import com.example.project.rm.version.Version;
import com.example.project.util.ElementInfo;
import com.example.project.util.SchemaUtils;
import com.example.project.util.StringUtils;
import com.sun.xml.xsom.XSSchema;
import com.sun.xml.xsom.XSSchemaSet;
import com.sun.xml.xsom.parser.XSOMParser;

@Service
public class XMLService {
	
	private Map<String, Class<? extends RMObject>> mappings;
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@SuppressWarnings("unchecked")
	private XMLService() throws ClassNotFoundException {
		mappings = new HashMap<>();
		
		ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
		scanner.addIncludeFilter(new AnnotationTypeFilter(RDFBean.class));
		
		for (BeanDefinition bd : scanner.findCandidateComponents("com.example.project.rm")) {
			Class<? extends RMObject> clazz = (Class<? extends RMObject>) Class.forName(bd.getBeanClassName());
			RDFBean anno = clazz.getDeclaredAnnotation(RDFBean.class);
			
			String name = anno.value().substring(anno.value().indexOf(":") + 1);
			mappings.put(name, clazz);
			
			log.info(name + " ~> " + clazz.getName());
		}
	}
	
	@SuppressWarnings("unchecked")
	public Version<Composition> parseVersion(String xml) throws Exception {
		return (Version<Composition>) parseXML(xml, "src/main/resources/xsd/Version.xsd");
	}

	public RMObject parseXML(String xml, String xsd) throws Exception {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(true);
		
		XSOMParser parser = new XSOMParser(factory);
		parser.parse(new File(xsd));
		
		XSSchemaSet schemas = parser.getResult();
		XSSchema schema = schemas.getSchema("http://schemas.openehr.org/v1");

		SAXReader reader = new SAXReader();
		Document document = reader.read(new StringReader(xml));
		Element root = document.getRootElement();	
		String type = root.attributeValue("type");
		
		return parseElement(root, type, schema);
	}
	
	private RMObject parseElement(Element root, String type, XSSchema schema) throws Exception {
		Class<? extends RMObject> clazz = mappings.get(type);		
		if (clazz == null)
			throw new Exception("No class found correspoding to RM type " + type);
		
		RMObject obj = clazz.newInstance();
		
		Map<String, List<Element>> map = new HashMap<>();
		for (Element child : root.elements()) {
			String name = child.getName();
			if (!map.containsKey(name)) {
				map.put(name, new ArrayList<>());
			}
			map.get(name).add(child);
		}
		
		for (String key : map.keySet()) {
			List<Object> values = new ArrayList<>();
			ElementInfo info = SchemaUtils.getElementInfo(type, key, schema);
			for (Element child : map.get(key)) {
				String childType = child.attributeValue("type");
				if (childType == null)
					childType = info.getType();
			
				if (childType.equals("string") || childType.equals("token") ||
					childType.equals("Iso8601DateTime") || childType.equals("Iso8601Date")) {
					values.add(child.getStringValue());
				} else if (childType.equals("double")) {
					values.add(Double.parseDouble(child.getStringValue()));
				} else if (childType.equals("int")) {
					values.add(Integer.parseInt(child.getStringValue()));
				} else {
					Object value = parseElement(child, childType, schema);
					if (value != null)
						values.add(value);
				}
			}
			if (values.size() > 0)
				setValue(obj, key, info.isRepeated() ? values : values.get(0));
		}
		
		for (Attribute attribute : root.attributes()) {
			setValue(obj, attribute.getName(), attribute.getText());
		}
		
		return obj;
	}

	private void setValue(Object object, String property, Object value) throws IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String fieldName = StringUtils.snakeCaseToCamelCase(property);
		PropertyDescriptor descriptor = getPropertyDescriptor(object.getClass(), fieldName);
		if (descriptor != null) {
			Method setter = descriptor.getWriteMethod();
			setter.invoke(object, value);
		}
	}
	
	private PropertyDescriptor getPropertyDescriptor(Class<?> clazz, String property) throws IntrospectionException {
		BeanInfo info = Introspector.getBeanInfo(clazz);
		for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
			if (pd.getName().equals(property))
				return pd;
		}
		return null;
	}
}

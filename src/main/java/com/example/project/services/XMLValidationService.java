package com.example.project.services;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.springframework.stereotype.Service;

@Service
public class XMLValidationService {

	private List<Exception> exceptions;
	
	public XMLValidationService() {
		exceptions = new ArrayList<>();
	}
	
	public boolean validateVersion(String xml) {
		return validate(xml, "src/main/resources/xsd/Version.xsd");
	}
	
	private boolean validate(String xml, String xsdPath) {
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		
		try {
		    Schema schema = factory.newSchema(new StreamSource(new File(xsdPath)));
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(new StringReader(xml)));
		} catch (Exception e) {
			e.printStackTrace();
			exceptions.add(e);
		}
		
		return exceptions.isEmpty();
	}

	public List<Exception> getErrors() {
		return exceptions;
	}
}

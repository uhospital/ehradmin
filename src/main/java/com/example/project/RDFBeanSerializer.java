package com.example.project;

import java.io.IOException;
import java.util.Set;

import org.cyberborean.rdfbeans.annotations.RDFBean;
import org.cyberborean.rdfbeans.exceptions.RDFBeanValidationException;
import org.cyberborean.rdfbeans.reflect.RDFBeanInfo;
import org.eclipse.rdf4j.model.IRI;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.BeanSerializerBuilder;
import com.fasterxml.jackson.databind.ser.impl.ObjectIdWriter;
import com.fasterxml.jackson.databind.ser.std.BeanSerializerBase;

public class RDFBeanSerializer extends BeanSerializerBase {

	private static final long serialVersionUID = -74754301107298200L;

	protected RDFBeanSerializer(BeanSerializerBase src) {
		super(src);
	}

	public RDFBeanSerializer(BeanSerializerBase src, ObjectIdWriter objectIdWriter, Object filterId) {
		super(src, objectIdWriter, filterId);
	}

	public RDFBeanSerializer(BeanSerializerBase src, ObjectIdWriter objectIdWriter) {
		super(src, objectIdWriter);
	}

	public RDFBeanSerializer(BeanSerializerBase src, Set<String> toIgnore) {
		super(src, toIgnore);
	}

	public RDFBeanSerializer(JavaType type, BeanSerializerBuilder builder, BeanPropertyWriter[] properties, BeanPropertyWriter[] filteredProperties) {
		super(type, builder, properties, filteredProperties);
	}

	@Override
	public void serialize(Object bean, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeStartObject();
		serializeFields(bean, gen, provider);
		
		Class<? extends Object> clazz = bean.getClass();
		RDFBean annotation = clazz.getAnnotation(RDFBean.class);
		if (annotation != null) {
			try {
				RDFBeanInfo beanInfo = RDFBeanInfo.get(clazz);
				IRI rdfType = beanInfo.getRDFType();
				gen.writeStringField("_type", rdfType.getLocalName());
			} catch (RDFBeanValidationException e) {
				e.printStackTrace();
			}			
		}
		
		gen.writeEndObject();
	}

	@Override
	public BeanSerializerBase withObjectIdWriter(ObjectIdWriter objectIdWriter) {
		return new RDFBeanSerializer(this, objectIdWriter);
	}

	@Override
	protected BeanSerializerBase withIgnorals(Set<String> toIgnore) {
		return new RDFBeanSerializer(this, toIgnore);
	}

	@Override
	protected BeanSerializerBase asArraySerializer() {
		return null;
	}

	@Override
	public BeanSerializerBase withFilterId(Object filterId) {
		return new RDFBeanSerializer(this, _objectIdWriter, filterId);
	}
}


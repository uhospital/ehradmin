package com.example.project.rm.common;

import java.util.ArrayList;
import java.util.List;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.base_types.DvIdentifier;

@RDFBean("common:PARTY_IDENTIFIED")
public class PartyIdentified extends PartyProxy {
	private String name;
	private List<DvIdentifier> identifiers;
	
	public PartyIdentified() {
		identifiers = new ArrayList<>();
	}

	@RDF("base_types:name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@RDF("base_types:identifiers")
	public List<DvIdentifier> getIdentifiers() {
		return identifiers;
	}

	public void setIdentifiers(List<DvIdentifier> identifiers) {
		this.identifiers = identifiers;
	}
}

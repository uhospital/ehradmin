package com.example.project.rm.common;

import org.cyberborean.rdfbeans.annotations.RDF;

import com.example.project.rm.RMObject;
import com.example.project.rm.base_types.PartyRef;

public abstract class PartyProxy extends RMObject {
	private PartyRef externalRef;

	@RDF("base_types:external_ref")
	public PartyRef getExternalRef() {
		return externalRef;
	}

	public void setExternalRef(PartyRef externalRef) {
		this.externalRef = externalRef;
	}
}

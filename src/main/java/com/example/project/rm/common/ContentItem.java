package com.example.project.rm.common;

import com.example.project.rm.structure.Locatable;

public abstract class ContentItem extends Locatable {

	public ContentItem(String nodeId) {
		super(nodeId);
	}

	public ContentItem() {
		super();
	}
}

package com.example.project.rm.common;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.structure.History;

@RDFBean("common:OBSERVATION")
public class Observation extends CareEntry {	
    private History data;
    
    public Observation() {
    		super();
    }

    public Observation(String nodeId) {
        super(nodeId);
    }

    public void setData(History data) {
        this.data = data;
    }
    
	@RDF("common:data")
    public History getData() {
        return data;
    }
}
package com.example.project.rm.common;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.structure.ItemStructure;

@RDFBean("ehr:EVALUATION")
public class Evaluation extends CareEntry {
	
	private ItemStructure data;

	public Evaluation() {
		super();
	}
	
	public Evaluation(String nodeId) {
		super(nodeId);
	}

	@RDF("ehr:data")
	public ItemStructure getData() {
		return data;
	}

	public void setData(ItemStructure data) {
		this.data = data;
	}
}

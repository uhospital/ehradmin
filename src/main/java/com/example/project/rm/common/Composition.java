package com.example.project.rm.common;

import java.util.ArrayList;
import java.util.List;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.base_types.CodePhrase;
import com.example.project.rm.base_types.DvCodedText;
import com.example.project.rm.structure.EventContext;
import com.example.project.rm.structure.Locatable;

@RDFBean("ehr:COMPOSITION")
public class Composition extends Locatable {
	private CodePhrase language;
	private CodePhrase territory;
	private DvCodedText category;
	private PartyProxy composer;
	private List<ContentItem> content;
	private EventContext context;
	
	public Composition() {
		super();
	}

	public Composition(String nodeId) {
		super(nodeId);
		this.content = new ArrayList<>();
	}

	@RDF("ehr:content")
	public List<ContentItem> getContent() {
		return content;
	}

	public void setContent(List<ContentItem> content) {
		this.content = content;
	}

	@RDF("common:language")
	public CodePhrase getLanguage() {
		return language;
	}

	public void setLanguage(CodePhrase language) {
		this.language = language;
	}

	@RDF("common:territory")
	public CodePhrase getTerritory() {
		return territory;
	}

	public void setTerritory(CodePhrase territory) {
		this.territory = territory;
	}

	@RDF("common:category")
	public DvCodedText getCategory() {
		return category;
	}

	public void setCategory(DvCodedText category) {
		this.category = category;
	}

	@RDF("common:composer")
	public PartyProxy getComposer() {
		return composer;
	}

	public void setComposer(PartyProxy composer) {
		this.composer = composer;
	}

	@RDF("common:context")
	public EventContext getContext() {
		return context;
	}

	public void setContext(EventContext context) {
		this.context = context;
	}
}

package com.example.project.rm.common;

import java.util.ArrayList;
import java.util.List;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

@RDFBean("content:SECTION")
public class Section extends ContentItem {
	private List<ContentItem> items;

	public Section() {
		super();
		items = new ArrayList<>();
	}

    @RDF("content:items")
	public List<ContentItem> getItems() {
		return items;
	}

	public void setItems(List<ContentItem> items) {
		this.items = items;
	}
}

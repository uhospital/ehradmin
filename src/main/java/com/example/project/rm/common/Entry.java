package com.example.project.rm.common;

public abstract class Entry extends ContentItem {

	public Entry(String nodeId) {
		super(nodeId);
	}

	public Entry() {
		super();
	}

}

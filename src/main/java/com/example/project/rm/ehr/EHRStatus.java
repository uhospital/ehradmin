package com.example.project.rm.ehr;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.common.PartySelf;
import com.example.project.rm.structure.Locatable;

@RDFBean("ehr:EHR_STATUS")
public class EHRStatus extends Locatable {

	private PartySelf subject;
	private boolean isQueryable;
	private boolean isModifiable; 
	
	public EHRStatus() {
		super(null);
	}
	
	public EHRStatus(String nodeId) {
		super(nodeId);
	}

	@RDF("ehr:subject")
	public PartySelf getSubject() {
		return subject;
	}

	public void setSubject(PartySelf subject) {
		this.subject = subject;
	}

	@RDF("ehr:is_queryable")
	public boolean isQueryable() {
		return isQueryable;
	}

	public void setQueryable(boolean isQueryable) {
		this.isQueryable = isQueryable;
	}

	@RDF("ehr:is_modifiable")
	public boolean isModifiable() {
		return isModifiable;
	}

	public void setModifiable(boolean isModifiable) {
		this.isModifiable = isModifiable;
	}
}

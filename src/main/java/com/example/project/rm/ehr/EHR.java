package com.example.project.rm.ehr;

import java.util.ArrayList;
import java.util.List;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;
import org.cyberborean.rdfbeans.annotations.RDFSubject;

import com.example.project.rm.base_types.DvDateTime;
import com.example.project.rm.base_types.HierObjectID;
import com.example.project.rm.base_types.ObjectRef;

@RDFBean("ehr:EHR")
public class EHR {
	private HierObjectID ehrId;
	private HierObjectID systemId;
	private DvDateTime timeCreated;
	private ObjectRef ehrStatus;
	private List<ObjectRef> compositions;
	private List<ObjectRef> contributions;
	private String subject;
	
	public EHR() {
		this.compositions = new ArrayList<>();
		this.contributions = new ArrayList<>();
	}

	@RDF("ehr:system_id")
	public HierObjectID getSystemId() {
		return systemId;
	}

	public void setSystemId(HierObjectID systemId) {
		this.systemId = systemId;
	}

	@RDF("ehr:time_created")
	public DvDateTime getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(DvDateTime timeCreated) {
		this.timeCreated = timeCreated;
	}

	@RDF("ehr:ehr_status")
	public ObjectRef getEhrStatus() {
		return ehrStatus;
	}

	public void setEhrStatus(ObjectRef ehrStatus) {
		this.ehrStatus = ehrStatus;
	}

	@RDF("ehr:compositions")
	public List<ObjectRef> getCompositions() {
		return compositions;
	}

	public void setCompositions(List<ObjectRef> compositions) {
		this.compositions = compositions;
	}

	@RDF("ehr:contributions")
	public List<ObjectRef> getContributions() {
		return contributions;
	}

	public void setContributions(List<ObjectRef> contributions) {
		this.contributions = contributions;
	}

	@RDFSubject(prefix="repo:")
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@RDF("ehr:ehr_id")
	public HierObjectID getEhrId() {
		return ehrId;
	}

	public void setEhrId(HierObjectID ehrId) {
		this.ehrId = ehrId;
	}
}

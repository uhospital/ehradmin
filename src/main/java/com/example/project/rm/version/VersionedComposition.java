package com.example.project.rm.version;

import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.common.Composition;

@RDFBean("version:VERSIONED_COMPOSITION")
public class VersionedComposition extends VersionedObject<Composition> {
}

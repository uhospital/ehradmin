package com.example.project.rm.version;

import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.ehr.EHRStatus;

@RDFBean("version:VERSIONED_EHR_STATUS")
public class VersionedEHRStatus extends VersionedObject<EHRStatus> {
}

package com.example.project.rm.version;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.base_types.ObjectVersionID;

@RDFBean("common:ORIGINAL_VERSION")
public class OriginalVersion<T> extends Version<T> {
	private ObjectVersionID uid;
	private T data;
	
	public OriginalVersion() {
	}

	public OriginalVersion(T data, ObjectVersionID uid) {
		this.data = data;
		this.uid = uid;
	}

	@RDF("common:data")
	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	@RDF("common:uid")
	public ObjectVersionID getUid() {
		return uid;
	}

	public void setUid(ObjectVersionID uid) {
		this.uid = uid;
		setSubject(uid.getValue());
	}
}

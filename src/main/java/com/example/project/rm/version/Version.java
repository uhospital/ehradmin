package com.example.project.rm.version;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFSubject;

import com.example.project.rm.RMObject;
import com.example.project.rm.base_types.AuditDetails;
import com.example.project.rm.base_types.ObjectRef;

public abstract class Version<T> extends RMObject {
	private ObjectRef contribution;
	private AuditDetails commitAudit;
	private String signature;
	private String subject;
	
	@RDF("common:contribution")
	public ObjectRef getContribution() {
		return contribution;
	}
	
	public void setContribution(ObjectRef contribution) {
		this.contribution = contribution;
	}
	
	@RDF("common:commit_audit")
	public AuditDetails getCommitAudit() {
		return commitAudit;
	}
	
	public void setCommitAudit(AuditDetails commitAudit) {
		this.commitAudit = commitAudit;
	}
	
	@RDF("common:signature")
	public String getSignature() {
		return signature;
	}
	
	public void setSignature(String signature) {
		this.signature = signature;
	}
	
	@RDFSubject(prefix="repo:")
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
}

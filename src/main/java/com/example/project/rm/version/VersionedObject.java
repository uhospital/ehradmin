package com.example.project.rm.version;

import java.util.ArrayList;
import java.util.List;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFSubject;

import com.example.project.rm.base_types.DvDateTime;
import com.example.project.rm.base_types.HierObjectID;
import com.example.project.rm.base_types.ObjectRef;
import com.example.project.rm.base_types.ObjectVersionID;

public class VersionedObject<T> {
	private HierObjectID uid;
	private String subject;
	private ObjectRef ownerId;
	private DvDateTime timeCreated;
	private List<Version<T>> versions;
	
	public VersionedObject() {
		this.versions = new ArrayList<>();
	}

	@RDF("version:uid")
	public HierObjectID getUID() {
		return uid;
	}

	public void setUID(HierObjectID uid) {
		this.uid = uid;
		this.subject = uid.getValue();
	}

	@RDFSubject(prefix="repo:")
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@RDF("common:versions")
	public List<Version<T>> getVersions() {
		return versions;
	}

	public void setVersions(List<Version<T>> versions) {
		this.versions = versions;
	}
	
	public void addVersion(Version<T> version) {
		this.versions.add(version);
	}

	public void commitOriginalVersion(T data, ObjectVersionID versionId) {
		OriginalVersion<T> version = new OriginalVersion<>(data, versionId);
		versions.add(version);
	}

	@RDF("common:owner_id")
	public ObjectRef getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(ObjectRef ownerId) {
		this.ownerId = ownerId;
	}

	@RDF("common:time_created")
	public DvDateTime getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(DvDateTime timeCreated) {
		this.timeCreated = timeCreated;
	}
}

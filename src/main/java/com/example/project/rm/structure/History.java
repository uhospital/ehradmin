package com.example.project.rm.structure;

import java.util.ArrayList;
import java.util.List;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

@RDFBean("structure:HISTORY")
public class History extends Locatable {
	
    private List<Event> events;
    
    public History() {
    	super();
    }

    public History(String nodeId) {
        super(nodeId);
        events = new ArrayList<Event>();
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }
    
	@RDF("structure:events")
    public List<Event> getEvents() {
        return events;
    }
}
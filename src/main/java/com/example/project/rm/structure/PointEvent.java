package com.example.project.rm.structure;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.base_types.DvDateTime;

@RDFBean("structure:POINT_EVENT")
public class PointEvent extends Event {
	
	private DvDateTime time;
	private ItemStructure state;

	public PointEvent() {
		super();
	}
	
	public PointEvent(String nodeId) {
		super(nodeId);
	}

	@RDF("structure:time")
	public DvDateTime getTime() {
		return time;
	}

	public void setTime(DvDateTime time) {
		this.time = time;
	}

	@RDF("structure:state")
	public ItemStructure getState() {
		return state;
	}

	public void setState(ItemStructure state) {
		this.state = state;
	}

}

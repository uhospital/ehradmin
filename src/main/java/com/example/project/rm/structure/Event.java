package com.example.project.rm.structure;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

@RDFBean("structure:EVENT")
public abstract class Event extends Locatable {
	
    private ItemTree data;

    public Event(String nodeId) {
        super(nodeId);
    }

    public Event() {
	}

	public void setData(ItemTree data) {
        this.data = data;
    }

	@RDF("structure:data")
    public ItemTree getData() {
        return data;
    }
}
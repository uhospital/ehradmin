package com.example.project.rm.structure;

import java.util.ArrayList;
import java.util.List;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

@RDFBean("structure:ITEM_TREE")
public class ItemTree extends ItemStructure {
	
    private List<Element> items;
    
    public ItemTree() {
    	super();
    	items = new ArrayList<>();
    }

    public ItemTree(String nodeId) {
        super(nodeId);
        items = new ArrayList<>();
    }

    public void setItems(List<Element> items) {
        this.items = items;
    }

	@RDF("structure:items")
    public List<Element> getItems() {
        return items;
    }
}
package com.example.project.rm.structure;

public abstract class ItemStructure extends Locatable {
	
	public ItemStructure() {
		super();
	}

	public ItemStructure(String nodeId) {
		super(nodeId);
	}
}

package com.example.project.rm.structure;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.RMObject;
import com.example.project.rm.base_types.ArchetypeID;
import com.example.project.rm.base_types.TemplateID;

@RDFBean("structure:ARCHETYPED")
public class Archetyped extends RMObject {

	private TemplateID templateId;
	private ArchetypeID archetypeId;
	private String rmVersion;
	
	public Archetyped() {
	}

	@RDF("structure:archetype_id")
	public ArchetypeID getArchetypeId() {
		return archetypeId;
	}

	public void setArchetypeId(ArchetypeID archetypeID) {
		this.archetypeId = archetypeID;
	}

	@RDF("structure:rm_version")
	public String getRmVersion() {
		return rmVersion;
	}

	public void setRmVersion(String rmVersion) {
		this.rmVersion = rmVersion;
	}

	@RDF("structure:template_id")
	public TemplateID getTemplateId() {
		return templateId;
	}

	public void setTemplateId(TemplateID templateID) {
		this.templateId = templateID;
	}
}

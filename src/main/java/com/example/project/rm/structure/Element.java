package com.example.project.rm.structure;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.base_types.DataValue;

@RDFBean("structure:ELEMENT")
public class Element extends Locatable {
	
    private DataValue value;
    
    public Element() {
    	super();
    }
    
    public Element(String nodeId) {
        super(nodeId);
    }

    public void setValue(DataValue value) {
        this.value = value;
    }

    @RDF("structure:value")
    public DataValue getValue() {
        return value;
    }
}
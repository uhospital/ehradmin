package com.example.project.rm.structure;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.RMObject;
import com.example.project.rm.base_types.DvCodedText;
import com.example.project.rm.base_types.DvDateTime;
import com.example.project.rm.base_types.Participation;
import com.example.project.rm.common.PartyIdentified;

@RDFBean("structure:EVENT_CONTEXT")
public class EventContext extends RMObject {
	private DvDateTime startTime;
	private DvDateTime endTime;
	private String location;
	private DvCodedText setting;
	private ItemStructure otherContext;
	private PartyIdentified healthCareFacility;
	private Participation participation;
	
	@RDF("structure:start_time")
	public DvDateTime getStartTime() {
		return startTime;
	}
	
	public void setStartTime(DvDateTime startTime) {
		this.startTime = startTime;
	}

	@RDF("structure:end_time")
	public DvDateTime getEndTime() {
		return endTime;
	}
	
	public void setEndTime(DvDateTime endTime) {
		this.endTime = endTime;
	}

	@RDF("structure:location")
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}

	@RDF("structure:setting")
	public DvCodedText getSetting() {
		return setting;
	}
	
	public void setSetting(DvCodedText setting) {
		this.setting = setting;
	}

	@RDF("structure:other_context")
	public ItemStructure getOtherContext() {
		return otherContext;
	}
	
	public void setOtherContext(ItemStructure otherContext) {
		this.otherContext = otherContext;
	}
	
	@RDF("structure:health_care_facility")
	public PartyIdentified getHealthCareFacility() {
		return healthCareFacility;
	}
	
	public void setHealthCareFacility(PartyIdentified healthCareFacility) {
		this.healthCareFacility = healthCareFacility;
	}

	@RDF("structure:participation")
	public Participation getParticipation() {
		return participation;
	}
	
	public void setParticipation(Participation participation) {
		this.participation = participation;
	}
}

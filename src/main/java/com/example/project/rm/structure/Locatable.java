package com.example.project.rm.structure;

import org.cyberborean.rdfbeans.annotations.RDF;

import com.example.project.rm.RMObject;
import com.example.project.rm.base_types.DvText;
import com.example.project.rm.base_types.UIDBasedID;

public abstract class Locatable extends RMObject {
	
	private UIDBasedID uid;
	private DvText name;
    private String archetypeNodeId;
    private Archetyped archetypeDetails;

    public Locatable(String nodeId) {
        this.archetypeNodeId = nodeId;
    }

    public Locatable() {
	}

	@RDF("structure:archetype_node_id")
    public String getArchetypeNodeId() {
        return archetypeNodeId;
    }

    public void setArchetypeNodeId(String nodeId) {
		this.archetypeNodeId = nodeId;
	}

	@RDF("structure:archetype_details")
	public Archetyped getArchetypeDetails() {
		return archetypeDetails;
	}

	public void setArchetypeDetails(Archetyped archetypeDetails) {
		this.archetypeDetails = archetypeDetails;
	}

	@RDF("structure:ouid")
	public UIDBasedID getUid() {
		return uid;
	}

	public void setUid(UIDBasedID uid) {
		this.uid = uid;
	}

	@RDF("structure:name")
	public DvText getName() {
		return name;
	}

	public void setName(DvText name) {
		this.name = name;
	}
}
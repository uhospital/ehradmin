@RDFNamespaces({
	"structure = http://unisinos.br/openehr/structure#",
	"common = http://unisinos.br/openehr/common#",
	"base_types = http://unisinos.br/openehr/base_types#",
	"ehr = http://unisinos.br/openehr/ehr#",
	"version = http://unisinos.br/openehr/version#",
	"repo = http://unisinos.br/openehr/repo#"
})
package com.example.project.rm;

import org.cyberborean.rdfbeans.annotations.RDFNamespaces;

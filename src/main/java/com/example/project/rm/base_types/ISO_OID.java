package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDFBean;

@RDFBean("base_types:ISO_OID")
public class ISO_OID extends UID {

	public ISO_OID() {
		super(null);
	}
	
	public ISO_OID(String value) {
		super(value);
	}
}

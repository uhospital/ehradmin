package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDF;

import com.example.project.rm.RMObject;

public abstract class ObjectID extends RMObject {
	private String value;
	
	public ObjectID() {
	}
	
	public ObjectID(String value) {
		this.value = value;
	}

	@RDF("base_types:value")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.RMObject;
import com.example.project.rm.common.PartyProxy;

@RDFBean("base_types:AUDIT_DETAILS")
public class AuditDetails extends RMObject {
	private String systemId;
	private PartyProxy committer;
	private DvDateTime timeCommited;
	private DvCodedText changeType;
	private DvText description;
	
	public AuditDetails() {
	}

	@RDF("base_types:system_id")
	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	@RDF("base_types:commiter")
	public PartyProxy getCommitter() {
		return committer;
	}

	public void setCommitter(PartyProxy commiter) {
		this.committer = commiter;
	}

	@RDF("base_types:time_committed")
	public DvDateTime getTimeCommitted() {
		return timeCommited;
	}

	public void setTimeCommitted(DvDateTime timeCommitted) {
		this.timeCommited = timeCommitted;
	}

	@RDF("base_types:change_type")
	public DvCodedText getChangeType() {
		return changeType;
	}

	public void setChangeType(DvCodedText changeType) {
		this.changeType = changeType;
	}

	@RDF("base_types:description")
	public DvText getDescription() {
		return description;
	}

	public void setDescription(DvText description) {
		this.description = description;
	}
}

package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

@RDFBean("base_types:DV_TEXT")
public class DvText extends DataValue {
	private String value;

	public DvText() {
	}

	public DvText(String value) {
		this.value = value;
	}

	@RDF("base_types:value")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

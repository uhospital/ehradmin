package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDFBean;

@RDFBean("base_types:HIER_OBJECT_ID")
public class HierObjectID extends UIDBasedID {
	
	public HierObjectID() {
	}
	
	public HierObjectID(UID uid) {
		super(uid.getValue());
	}

	public HierObjectID(String value) {
		super(value);
	}

	public HierObjectID(String root, String extension) {
		super(root + "::" + extension);
	}
}

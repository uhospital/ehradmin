package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.RMObject;

@RDFBean("base_types:VERSION_TREE_ID")
public class VersionTreeID extends RMObject {
	private String value;

	public VersionTreeID(String value) {
		this.value = value;
	}
	
	@RDF("base_types:value")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDFBean;

@RDFBean("base_types:ARCHETYPE_ID")
public class ArchetypeID extends ObjectID {
	
	public ArchetypeID() {
	}
	
	public ArchetypeID(String value) {
		super(value);
	}
}

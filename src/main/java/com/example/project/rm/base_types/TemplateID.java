package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDFBean;

@RDFBean("base_types:TEMPLATE_ID")
public class TemplateID extends ObjectID {
	
	public TemplateID() {
	}
	
	public TemplateID(String value) {
		super(value);
	}
}

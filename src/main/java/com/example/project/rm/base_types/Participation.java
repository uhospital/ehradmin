package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.RMObject;
import com.example.project.rm.common.PartyProxy;

@RDFBean("base_types:PARTICIPATION")
public class Participation extends RMObject {
	private DvText function;
	private PartyProxy performer;
	private DvInterval time;
	private DvCodedText mode;

	@RDF("base_types:function")
	public DvText getFunction() {
		return function;
	}
	
	public void setFunction(DvText function) {
		this.function = function;
	}
	
	@RDF("base_types:performer")
	public PartyProxy getPerformer() {
		return performer;
	}
	
	public void setPerformer(PartyProxy performer) {
		this.performer = performer;
	}
	
	@RDF("base_types:time")
	public DvInterval getTime() {
		return time;
	}
	
	public void setTime(DvInterval time) {
		this.time = time;
	}
	
	@RDF("base_types:mode")
	public DvCodedText getMode() {
		return mode;
	}
	
	public void setMode(DvCodedText mode) {
		this.mode = mode;
	}
}

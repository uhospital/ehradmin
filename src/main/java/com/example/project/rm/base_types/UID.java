package com.example.project.rm.base_types;

import com.example.project.rm.RMObject;

public abstract class UID extends RMObject {
	private String value;

	public UID(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}

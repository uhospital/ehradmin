package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.RMObject;

@RDFBean("base_types:OBJECT_REF")
public class ObjectRef extends RMObject {
	private String namespace;
	private String type;
	private ObjectID id;
	
	public ObjectRef() {
	}
	

	public ObjectRef(String namespace, String type, ObjectID id) {
		this.namespace = namespace;
		this.type = type;
		this.id = id;
	}

	@RDF("base_types:namespace")
	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	@RDF("base_types:type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@RDF("base_types:id")
	public ObjectID getId() {
		return id;
	}

	public void setId(ObjectID id) {
		this.id = id;
	}
}

package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

@RDFBean("base_types:DV_QUANTITY")
public class DvQuantity extends DataValue {	
    private String units;
    private Double magnitude;
    private Integer precision;
    
    public DvQuantity() {
    }
    
    public DvQuantity(String units, double magnitude, int precision) {
        this.units = units;
        this.magnitude = magnitude;
        this.precision = precision;
    }

	@RDF("base_types:unit")
    public String getUnits() {
        return units;
    }
	
	public void setUnits(String units) {
		this.units = units;
	}

    @RDF("base_types:magnitude")
    public Double getMagnitude() {
        return magnitude;
    }
    
	public void setMagnitude(Double magnitude) {
		this.magnitude = magnitude;
	}

    @RDF("base_types:precision")
    public Integer getPrecision() {
        return precision;
    }
    
	public void setPrecision(Integer precision) {
		this.precision = precision;
	}
}
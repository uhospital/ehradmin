package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDFBean;

@RDFBean("base_types:OBJECT_VERSION_ID")
public class ObjectVersionID extends UIDBasedID {
	private UID objectId;
	private VersionTreeID versionTreeId;
	private HierObjectID creatingSystemId;
	
	public ObjectVersionID() {
		super();
	}

	public ObjectVersionID(String value) {
		super(value);
		loadValue(value);
	}

	public ObjectVersionID(String uid, String systemId, String version) {
		super(uid + "::" + systemId + "::" + version);
	}
	
	public void setValue(String value) {
		super.setValue(value);
		loadValue(value);
	}
	
	private void loadValue(String value) {
		String[] splits = value.split("::");
		
		if (splits[0].matches("([0-9A-Fa-f])+(-(([0-9A-Fa-f]))+)*")) {
			objectId = new UUID(splits[0]);
		} else if (splits[0].matches("(\\d)+(\\.(\\d)+)*")) {
			objectId = new ISO_OID(splits[0]);
		} else if (splits[0].matches("(\\w|-)+(\\.(\\w|-)+)*")) {
			objectId = new InternetID(splits[0]);
		}
		
		creatingSystemId = new HierObjectID(splits[1]);
		versionTreeId = new VersionTreeID(splits[2]);
	}

	public UID getObjectId() {
		return objectId;
	}

	public VersionTreeID getVersionTreeId() {
		return versionTreeId;
	}

	public HierObjectID getCreatingSystemId() {
		return creatingSystemId;
	}
}

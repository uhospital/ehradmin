package com.example.project.rm.base_types;

public abstract class UIDBasedID extends ObjectID {

	public UIDBasedID(String value) {
		super(value);
	}

	public UIDBasedID() {
	}
}

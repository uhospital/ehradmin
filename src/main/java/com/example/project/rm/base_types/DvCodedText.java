package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

@RDFBean("base_types:DV_CODED_TEXT")
public class DvCodedText extends DvText {
	private CodePhrase definingCode;

	@RDF("base_types:defining_code")
	public CodePhrase getDefiningCode() {
		return definingCode;
	}

	public void setDefiningCode(CodePhrase definingCode) {
		this.definingCode = definingCode;
	}
}

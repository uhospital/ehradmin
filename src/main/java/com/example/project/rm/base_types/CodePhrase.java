package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.RMObject;

@RDFBean("base_types:CODE_PHRASE")
public class CodePhrase extends RMObject {
	private TerminologyID terminologyId;
	private String codeString;
	
	@RDF("base_types:terminology_id")
	public TerminologyID getTerminologyId() {
		return terminologyId;
	}
	
	public void setTerminologyId(TerminologyID terminologyId) {
		this.terminologyId = terminologyId;
	}
	
	@RDF("base_types:code_string")
	public String getCodeString() {
		return codeString;
	}
	
	public void setCodeString(String codeString) {
		this.codeString = codeString;
	}
}

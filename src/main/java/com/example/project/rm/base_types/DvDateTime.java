package com.example.project.rm.base_types;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

@RDFBean("structure:DV_DATE_TIME")
public class DvDateTime extends DataValue {
	private String value;
	
	public DvDateTime() {
	}
	
	public DvDateTime(ZonedDateTime date) {
		value = date.format(DateTimeFormatter.ISO_INSTANT);
	}

	@RDF("structure:value")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

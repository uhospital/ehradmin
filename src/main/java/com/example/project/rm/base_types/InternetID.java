package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDFBean;

@RDFBean("base_types:INTERNET_ID")
public class InternetID extends UID {

	public InternetID() {
		super(null);
	}
	
	public InternetID(String value) {
		super(value);
	}
}

package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDFBean;

@RDFBean("base_types:UUID")
public class UUID extends UID {

	public UUID() {
		super(null);
	}
	
	public UUID(String value) {
		super(value);
	}
}

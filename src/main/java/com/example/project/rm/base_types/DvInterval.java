package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

@RDFBean("base_types:DV_INTERVAL")
public class DvInterval extends DataValue {
	private DvOrdered lower;
	private DvOrdered upper;
	private boolean lowerIncluded;
	private boolean upperIncluded;
	private boolean lowerUnbounded;
	private boolean upperUnbounded;
	
	@RDF("base_types:lower")
	public DvOrdered getLower() {
		return lower;
	}
	public void setLower(DvOrdered lower) {
		this.lower = lower;
	}
	
	@RDF("base_types:upper")
	public DvOrdered getUpper() {
		return upper;
	}
	
	public void setUpper(DvOrdered upper) {
		this.upper = upper;
	}
	
	@RDF("base_types:lower_included")
	public boolean isLowerIncluded() {
		return lowerIncluded;
	}
	
	public void setLowerIncluded(boolean lowerIncluded) {
		this.lowerIncluded = lowerIncluded;
	}

	@RDF("base_types:upper_included")
	public boolean isUpperIncluded() {
		return upperIncluded;
	}
	
	public void setUpperIncluded(boolean upperIncluded) {
		this.upperIncluded = upperIncluded;
	}
	
	@RDF("base_types:lower_unbounded")
	public boolean isLowerUnbounded() {
		return lowerUnbounded;
	}
	
	public void setLowerUnbounded(boolean lowerUnbounded) {
		this.lowerUnbounded = lowerUnbounded;
	}
	
	@RDF("base_types:upper_unbounded")
	public boolean isUpperUnbounded() {
		return upperUnbounded;
	}
	
	public void setUpperUnbounded(boolean upperUnbounded) {
		this.upperUnbounded = upperUnbounded;
	}
}

package com.example.project.rm.base_types;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;

import com.example.project.rm.common.PartyProxy;

@RDFBean("base_types:DV_IDENTIFIER")
public class DvIdentifier extends PartyProxy {
	private String issuer;
	private String assigner;
	private String id;
	private String type;
	
	@RDF("base_types:issuer")
	public String getIssuer() {
		return issuer;
	}
	
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	
	@RDF("base_types:assigner")
	public String getAssigner() {
		return assigner;
	}
	
	public void setAssigner(String assigner) {
		this.assigner = assigner;
	}
	
	@RDF("base_types:id")
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	@RDF("base_types:type")
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
}

package com.example.project.controllers;

import java.beans.PropertyDescriptor;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.ClassPathUtils;
import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.apache.commons.text.WordUtils;
import org.cyberborean.rdfbeans.RDFBeanManager;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.openehr.am.archetype.Archetype;
import org.openehr.am.archetype.constraintmodel.CAttribute;
import org.openehr.am.archetype.constraintmodel.CComplexObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.complexible.stardog.rdf4j.StardogRepository;
import com.example.project.rm.RMObject;
import com.example.project.rm.base_types.ArchetypeID;
import com.example.project.rm.base_types.DvQuantity;
import com.example.project.rm.base_types.HierObjectID;
import com.example.project.rm.base_types.ObjectVersionID;
import com.example.project.rm.common.Composition;
import com.example.project.rm.common.ContentItem;
import com.example.project.rm.common.Observation;
import com.example.project.rm.structure.Archetyped;
import com.example.project.rm.structure.Element;
import com.example.project.rm.structure.Locatable;
import com.example.project.rm.version.OriginalVersion;
import com.example.project.rm.version.VersionedComposition;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CorpusController;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.util.spring.SavedApplicationFactoryBean;
import se.acode.openehr.parser.ADLParser;
import se.acode.openehr.parser.ParseException;

@RestController
@RequestMapping("/gate")
@SuppressWarnings("unchecked")
public class GateController {

	@Autowired
	private StardogRepository repo;
	
	@Autowired
	private SavedApplicationFactoryBean factoryBean;
	
	private final Map<String, RMObject> instances = new HashMap<>();

	@RequestMapping(value="/observation", method=RequestMethod.POST)
	public void observation(@RequestBody String content) throws Exception {
		CorpusController controller = (CorpusController) factoryBean.getObject();
		
		Document document = Factory.newDocument(content);
		Corpus corpus = Factory.newCorpus("corpus");
		corpus.add(document);
		
		controller.setCorpus(corpus);
		controller.execute();

		String archetypeName = "openEHR-EHR-OBSERVATION.blood_pressure.v1.adl";
		Archetype archetype = parseArchetype(archetypeName);
		Observation observation = new Observation(archetypeName);

		Archetyped details = new Archetyped();
		details.setArchetypeId(new ArchetypeID(archetypeName));
		details.setRmVersion("1.0");
		observation.setArchetypeDetails(details);
		
		Map<String, String> mappings = new HashMap<>();
		mappings.put("Systolic", "data[at0001]/events[at0006]/data[at0003]/items[at0004]");
		mappings.put("Diastolic", "data[at0001]/events[at0006]/data[at0003]/items[at0005]");

		AnnotationSet inferredAnnotations = document.getAnnotations("LearningFramework");
		AnnotationSet defaultAnnotations = document.getAnnotations();
		
		for (String annotationType : mappings.keySet()) {
			Annotation inferredAnnotation = getAnnotationWithType(annotationType, inferredAnnotations);
			Annotation quantityAnnotation = getQuantityAnnotation(inferredAnnotation, defaultAnnotations);

			FeatureMap fm = quantityAnnotation.getFeatures();
			String unit = (String) fm.get("unit");
			Double value = Double.parseDouble((String) fm.get("value"));
			DvQuantity quantity = new DvQuantity(unit, value, 0);

			String path = mappings.get(annotationType);
			Element element = (Element) createRmTypeFromPath(path, observation, archetype);
			element.setValue(quantity);
		}
		
		try (RepositoryConnection conn = repo.getConnection()) {
			VersionedComposition obj = wrapIntoVersionedComposition(observation);
			RDFBeanManager manager = new RDFBeanManager(conn);
			manager.add(obj);
		}
	}

	private VersionedComposition wrapIntoVersionedComposition(ContentItem item) {
		VersionedComposition versionedComposition = new VersionedComposition();
		versionedComposition.setUID(new HierObjectID("1.2.3.4"));
		
		OriginalVersion<Composition> originalVersion = new OriginalVersion<>();
		originalVersion.setUid(new ObjectVersionID("1.2.3.4::br.unisinos.app::1.0.0"));
		
		Composition composition = new Composition(null);
		composition.getContent().add(item);
		originalVersion.setData(composition);
		
		versionedComposition.addVersion(originalVersion);
		return versionedComposition;
	}

	private Annotation getAnnotationWithType(String type, AnnotationSet annotations) {
		AnnotationSet foundAnnotations = annotations.get(type);
		Iterator<Annotation> it = foundAnnotations.iterator();
		return it.next();
	}

	private Annotation getQuantityAnnotation(Annotation otherAnnotation, AnnotationSet annotations) {
		Long startOffset = otherAnnotation.getStartNode().getOffset();
		Long endOffset = otherAnnotation.getEndNode().getOffset();
		AnnotationSet quantityAnnotations = annotations.getCovering("Quantity", startOffset, endOffset);
		Iterator<Annotation> it = quantityAnnotations.iterator();
		return it.next();
	}

	private Archetype parseArchetype(String archetype) throws ParseException, Exception {
		File file = new File(archetype);
		ADLParser parser = new ADLParser(file);
		return parser.archetype();
	}

	private RMObject createRmTypeFromPath(String path, RMObject parent, Archetype archetype) throws Exception {
		CComplexObject definition = archetype.getDefinition();
		RMObject theInstance = parent;
		CComplexObject child = definition;
		List<String> segments = convertPathIntoSegments(path);
		StringBuilder sb = new StringBuilder();
		for (String segment : segments) {
			String attributeName = null;
			String nodeId = null;
			int index = segment.indexOf("[");
			if (index > 0) {
				attributeName = segment.substring(0, index);
				nodeId = segment.substring(index + 1, segment.indexOf("]"));
			} else {
				attributeName = segment;
			}

			CAttribute attribute = getAttributeByName(child, attributeName);
			child = getChildFromAttribute(attribute, nodeId);

			RMObject newInstance;
			sb.append(sb.length() > 0 ? "/" + segment : segment);
			String currentPath = sb.toString();
			if (instances.containsKey(currentPath)) {
				newInstance = instances.get(currentPath);
			} else {
				newInstance = createRmTypeFromComplexObject(child);
				setAttributeValue(theInstance, attributeName, newInstance);
				instances.put(currentPath, newInstance);
			}

			theInstance = newInstance;
		}

		return theInstance;
	}

	private List<String> convertPathIntoSegments(String path) {
		List<String> segments = new ArrayList<>();
		StringTokenizer tokens = new StringTokenizer(path, "/");
		while (tokens.hasMoreTokens()) {
			segments.add(tokens.nextToken());
		}
		return segments;
	}

	private CAttribute getAttributeByName(CComplexObject child, String attributeName) {
		return child.getAttributes()
				.stream()
				.filter(a -> a.getRmAttributeName().equals(attributeName))
				.findFirst().get();
	}

	private CComplexObject getChildFromAttribute(CAttribute attribute, String objectIdentifier) throws Exception {
		if (attribute.getChildren().size() > 1) {
			if (objectIdentifier == null)
				throw new Exception("Ambiguous attribute");

			return (CComplexObject) attribute.getChildren()
					.stream()
					.filter(c -> c.getNodeId().equals(objectIdentifier))
					.findFirst().get();
		} else {
			return (CComplexObject) attribute.getChildren()
					.stream()
					.findFirst().get();
		}
	}

	private Locatable createRmTypeFromComplexObject(CComplexObject object) throws Exception {
		Class<Locatable> clazz = getClassFromRmType(object.getRmTypeName());
		return ConstructorUtils.invokeConstructor(clazz, object.getNodeId());
	}

	private void setAttributeValue(RMObject theInstance, String attributeName, RMObject newInstance) throws Exception {
		PropertyDescriptor descriptor = PropertyUtils.getPropertyDescriptor(theInstance, attributeName);
		if (Collection.class.isAssignableFrom(descriptor.getPropertyType())) {
			Collection<Object> values = (Collection<Object>) PropertyUtils.getSimpleProperty(theInstance, attributeName);
			values.add(newInstance);
			PropertyUtils.setSimpleProperty(theInstance, attributeName, values);
		} else {
			PropertyUtils.setSimpleProperty(theInstance, attributeName, newInstance);
		}
	}

	private Class<Locatable> getClassFromRmType(String rmType) throws ClassNotFoundException {
		String className = WordUtils.capitalizeFully(rmType, "_".toCharArray());
		className = className.replace("_", "");

		String classNameWithPackage = ClassPathUtils.toFullyQualifiedName(Locatable.class, className);
		return (Class<Locatable>) Class.forName(classNameWithPackage);
	}
}

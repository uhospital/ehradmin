package com.example.project.controllers;

import java.lang.reflect.InvocationTargetException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.cyberborean.rdfbeans.RDFBeanManager;
import org.cyberborean.rdfbeans.exceptions.RDFBeanException;
import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.complexible.stardog.rdf4j.StardogRepository;
import com.complexible.stardog.security.ResourceNotFoundException;
import com.example.project.rm.base_types.DvDateTime;
import com.example.project.rm.base_types.HierObjectID;
import com.example.project.rm.base_types.ObjectRef;
import com.example.project.rm.common.Composition;
import com.example.project.rm.ehr.EHR;
import com.example.project.rm.version.OriginalVersion;
import com.example.project.rm.version.Version;
import com.example.project.rm.version.VersionedComposition;
import com.example.project.services.XMLService;
import com.example.project.services.XMLValidationService;

@RestController
@RequestMapping("/api")
public class RESTController {

	private static final String SYSTEM_ID = "br.unisinos.app";

	@Autowired
	private StardogRepository repo;
	
	@Autowired
	private XMLValidationService validationService;
	
	@Autowired
	private XMLService xmlService;
	
	@RequestMapping(value="/ehrs")
	public List<String> getAllEHRs() throws Exception {
		try (RepositoryConnection conn = repo.getConnection()) {
			RDFBeanManager manager = new RDFBeanManager(conn);
			CloseableIteration<EHR,Exception> it = manager.getAll(EHR.class);
			List<String> ehrs = new ArrayList<>();
			while (it.hasNext()) {
				ehrs.add(it.next().getSubject());
			}
			return ehrs;
		}
	}
	
	@RequestMapping(value="/ehr", method=RequestMethod.POST)
	public ResponseEntity<EHR> createEHR() throws RepositoryException, RDFBeanException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {		
		ZonedDateTime now = ZonedDateTime.now();
		String uuid = UUID.randomUUID().toString();
		
		EHR ehr = new EHR();
		ehr.setSubject(uuid);
		ehr.setSystemId(new HierObjectID(SYSTEM_ID));
		ehr.setTimeCreated(new DvDateTime(now));

		try (RepositoryConnection conn = repo.getConnection()) {
			RDFBeanManager manager = new RDFBeanManager(conn);
			manager.add(ehr);
		}
		
		return new ResponseEntity<EHR>(ehr, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/ehr/{ehrId}")
	public EHR getEHR(@PathVariable String ehrId) throws RDF4JException, RDFBeanException {
		try (RepositoryConnection conn = repo.getConnection()) {
			RDFBeanManager manager = new RDFBeanManager(conn);
			EHR ehr = manager.get(ehrId, EHR.class);
			
			if (ehr == null)
				throw new ResourceNotFoundException("EHR not found");
			
			return ehr;
		}
	}
	
	@RequestMapping(value="/ehr/{ehrId}/versioned_composition/{versinedObjectUid}")
	public VersionedComposition getVersionedComposition(@PathVariable String ehrId, @PathVariable String versinedObjectUid) {
		return null;
	}
	
	@RequestMapping(value="/ehr/{ehrId}/composition", method=RequestMethod.POST)
	public ResponseEntity<Object> commitComposition(@PathVariable String ehrId, @RequestBody String xml) throws Exception {		
		if (!validationService.validateVersion(xml)) {
			return new ResponseEntity<Object>(validationService.getErrors(), HttpStatus.BAD_REQUEST);
		}
		
		try (RepositoryConnection conn = repo.getConnection()) {
			RDFBeanManager manager = new RDFBeanManager(conn);
			Version<Composition> version = xmlService.parseVersion(xml);
			ZonedDateTime now = ZonedDateTime.now();
			
			if (version instanceof OriginalVersion<?>) {
				OriginalVersion<?> originalVersion = (OriginalVersion<?>) version;
				originalVersion.setSubject(originalVersion.getUid().getValue());
				
				VersionedComposition versionedComposition = new VersionedComposition();
				versionedComposition.setUID(new HierObjectID(originalVersion.getUid().getObjectId()));
				versionedComposition.setOwnerId(new ObjectRef("LOCAL", "EHR", new HierObjectID(ehrId)));
				versionedComposition.setTimeCreated(new DvDateTime(now));
				versionedComposition.addVersion(version);
				
				EHR ehr = manager.get(ehrId, EHR.class);
				if (ehr == null)
					throw new ResourceNotFoundException("EHR not found");
				
				ehr.getContributions().add(version.getContribution());
				ehr.getCompositions().add(new ObjectRef("LOCAL", "VERSIONED_COMPOSITION", versionedComposition.getUID()));
				
				manager.add(versionedComposition);
				manager.update(ehr);
				
				return new ResponseEntity<Object>(originalVersion.getData(), HttpStatus.CREATED);
			} else {
				throw new Exception("IMPORTED_VERSION is not supported");
			}
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value="/ehr/{ehrId}/composition/{versionUid}")
	public Composition getComposition(@PathVariable String ehrId, @PathVariable String versionUid) throws RDF4JException, RDFBeanException {
		try (RepositoryConnection conn = repo.getConnection()) {
			RDFBeanManager manager = new RDFBeanManager(conn);
			OriginalVersion<Composition> originalVersion = manager.get(versionUid, OriginalVersion.class);
			
			// TODO: check if the VERSION uid belongs to that EHR
			
			if (originalVersion == null)
				throw new ResourceNotFoundException();
			
			return originalVersion.getData();
		}
	}
}

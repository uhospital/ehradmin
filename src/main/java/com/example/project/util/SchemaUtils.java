package com.example.project.util;

import com.sun.xml.xsom.XSComplexType;
import com.sun.xml.xsom.XSContentType;
import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSModelGroup;
import com.sun.xml.xsom.XSParticle;
import com.sun.xml.xsom.XSSchema;
import com.sun.xml.xsom.XSTerm;

public class SchemaUtils {
	
	public static ElementInfo getElementInfo(String type, String name, XSSchema schema) {
		XSComplexType complexType = schema.getComplexType(type);
		XSContentType contentType = complexType.getContentType();
		XSParticle particle = contentType.asParticle();
		if (particle != null) {
			return getElementInfoRecursively(particle, name);
		}
		return null;
	}

	private static ElementInfo getElementInfoRecursively(XSParticle particle, String elementName) {
		XSTerm term = particle.getTerm();
		if (term.isElementDecl()) {
			XSElementDecl element = term.asElementDecl();
			if (element.getName().equals(elementName))
				return new ElementInfo(element.getType().getName(), particle.isRepeated());
		} else if (term.isModelGroup()) {
			XSModelGroup modelGroup = term.asModelGroup();
			XSParticle[] particles = modelGroup.getChildren();
			for (XSParticle p : particles) {
				ElementInfo info = getElementInfoRecursively(p, elementName);
				if (info != null)
					return info;
			}
		}
		return null;
	}
}

package com.example.project.util;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ElementUtils {
	
	public static List<Element> getChildElements(Element root) {
		List<Element> elements = new ArrayList<>();
		NodeList children = root.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node node = children.item(i);	
			if (node.getNodeType() == Node.ELEMENT_NODE)
				elements.add((Element) node);
		}
		return elements;
	}
	
	public static Element getChildByTagName(Element root, String tagName) {
		NodeList children = root.getChildNodes();
		if (children.getLength() > 0) {
			return (Element) children.item(0);
		} else {
			return null;
		}
	}
}

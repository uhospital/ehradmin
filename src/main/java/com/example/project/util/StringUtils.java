package com.example.project.util;

import java.util.StringTokenizer;

public class StringUtils {

	public static String snakeCaseToCamelCase(String value) {
		StringTokenizer tokens = new StringTokenizer(value, "_");
		StringBuilder sb = new StringBuilder(tokens.nextToken());
		while (tokens.hasMoreTokens()) {
			String token = tokens.nextToken();
			sb.append(Character.toUpperCase(token.charAt(0)) + token.substring(1));
		}
		return sb.toString();
	}
}

package com.example.project.util;

public class ElementInfo {
	private String type;
	private boolean repeated;
	
	public ElementInfo(String type, boolean repeated) {
		this.type = type;
		this.repeated = repeated;
	}

	public String getType() {
		return type;
	}

	public boolean isRepeated() {
		return repeated;
	}
}

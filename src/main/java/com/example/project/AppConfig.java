package com.example.project;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.complexible.stardog.api.ConnectionConfiguration;
import com.complexible.stardog.rdf4j.StardogRepository;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.module.SimpleModule;

@Configuration
@ImportResource("file:src/main/resources/spring-beans.xml")
public class AppConfig extends WebMvcConfigurerAdapter {
	
	@Bean
	public StardogRepository stardogRepository() {
		ConnectionConfiguration config = ConnectionConfiguration
				.to("myDb")
				.server("http://localhost:5820")
				.credentials("admin", "admin");
		StardogRepository repo = new StardogRepository(config);
		return repo;
	}
	
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		SimpleModule module = new SimpleModule();
		module.setSerializerModifier(new CustomBeanSerializerModifier());
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		objectMapper.registerModule(module);
		
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setObjectMapper(objectMapper);
		converters.add(converter);
		
		super.configureMessageConverters(converters);
	}
}
